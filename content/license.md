+++
date = "2017-05-13T22:53:34+02:00"
description = "Desc"
meta_img = "/images/image.jpg"
tags = ["License"]
title = "License"

+++


Sauf mention contraire, les contenus de ce blog sont publiés sous license [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

Ce site est généré via [gohugo](https://gohugo.io/).

Le thème utilisé est [cocoa-eh](https://github.com/fuegowolf/cocoa-eh-hugo-theme).

Plusieurs icônes proviennent de [font awesome](http://fontawesome.io/).

Icônes depuis le site [flaticon](http://www.flaticon.com/) :

 * Icône de l'asterik est sous license [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/) par [Dave Gandy](http://www.flaticon.com/authors/dave-gandy).

 * Icône de l'éléphant est sous license [Flaticon Basic License](http://file000.flaticon.com/downloads/license/license.pdf) par [Freepik](http://www.flaticon.com/authors/freepik).
