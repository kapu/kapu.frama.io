+++
date = "2017-08-22T14:44:00+02:00"
meta_img = "/images/image.jpg"
tags = ["tags"]
description = "Desc"
title = "Maisons de santé: un exemple de commun en santé?"
draft = false
+++


Au travers de cet article je vais essayer d'explorer la notion de maison de santé et voir dans quelles mesures on peut la considérer comme un bien commun en santé.

                               

Nous allons commencer par le début (O_o) et définir ce que sont une maison , un pôle et un centre de santé.


## Différencier les structures

Première grande question à laquelle répondre: **qu'est-ce qu'une maison de santé?**

### a. Les maisons de santé
 
Selon [le code de santé publique](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006072665&idArticle=LEGIARTI000017744182&dateTexte=&categorieLien=cid):

>Art. L. 6323-3. - La maison de santé est une personne morale constituée entre des professionnels médicaux, auxiliaires médicaux ou pharmaciens.

Alors toutes les structures regroupant plusieurs professionnels de santé sont des maisons de santé? 
Bien qu'il n'y ai pas de "label", tout regroupement de professionel de santé peut s'auto-proclamer maison de santé. Cependant, si des financements publics (via les Antennes Régionales de Santé par exemple) sont requis, leurs acceptations se fera si la structure répond aux critères énoncés par la [DGOS](http://solidarites-sante.gouv.fr/ministere/organisation/directions/article/dgos-direction-generale-de-l-offre-de-soins):

  * au moins deux médecins
  * au moins un professionnel paramédical
  * ces professionnels réunis dans la structure partagent un projet de santé pour la population alentours.

La maison de santé est donc un regroupement de professionels de santé rassemblés au sein d'une même structure et d'un **[projet](https://www.youtube.com/watch?v=4VvWN_G43x0) de santé commun**.
<div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/7Q7SqFSRmzkFq" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/franceinfo-citation-emmanuel-macron-7Q7SqFSRmzkFq">via GIPHY</a></p>

La [FFMPS](http://www.ffmps.fr/)(Fédération Française des maisons et pôles de santé), ainsi que le [rapport de 2009](http://solidarites-sante.gouv.fr/IMG/pdf/rapport_maison_de_sante.pdf) sur les pôles et maisons de santé mettent aussi en avant l'importance de leaders pour mettre en place ce type de structure.


### b. Les pôles de santé


> Le pôle regroupe dans un même territoire des professionnels de santé différents, chacun conservant son indépendance et son lieu d’exercice. La loi HPST a reconnu son existence mais ne lui impose pas, contrairement aux maisons et centres de santé, l’obligation d’élaborer un projet de santé.

> **Le bilan des maisons et des pôles de santé et Les propositions pour leur déploiement (2009)**

Les pôles de santé s'appuient ainsi sur l'existant et visent à optimiser, mutualiser les moyens pour une meilleure coordination et un travail plus efficient à l'échelle d'un territoire. Chaque professionnel garde son indépendance vis à vis de ses collègues.


### c. Les centres de santé
 
 Les centres de santés sont les plus anciens modes d'exercice pluri-professionnel en France, avec une forte dimension sociale dans ses missions. Ces centres sont gérés par des organismes à but non lucratifs (mutuelles, etc.) ou des collectivités locales.

 Leurs but est de garantir un accès au soin, en pratiquant le tiers payant, ainsi qu'en y inscrivant les professionnels de santé en secteur 1.

 Bien qu'il y ai un projet commun au sein de ces centres, il n'émane pas des professionnels qui y travaillent.
 
**Dans le reste de l'article je ne vais plus parler que des maisons de santé.**

## Les limites

Plusieurs limites à ces modèles existent:

 * non valorisation de certaines missions de santé publique et de prévention
 * modèle de rémunération non adapté ou attractif (notamment pour les infirmier-e-s).
 * modèle juridique pas forcément adapté .
 * Coût d'investissement important au sein des maisons de santé. Ce surcoût est évalué, en moyenne, à 4400 euros annuels par médecin exerçant en maison de santé [(rapport de 2009)](http://solidarites-sante.gouv.fr/IMG/pdf/rapport_maison_de_sante.pdf).
 * Absence de dossier de patient partagé.


## Qu'est-ce qu'un commun?

Deuxième grande question de cette article: **qu'est-ce qu'un commun?**


{{< youtube qrgtbgjMfu0 >}}



Pour commencer, je vais reprendre la définition proposée sur le [portail des communs](http://lescommuns.org/).

>Les biens communs, ou tout simplement communs, sont des ressources, gérées collectivement par une communauté, celle-ci établit des règles et une gouvernance dans le but de préserver et pérenniser cette ressource. 
Des logiciels libres aux jardins partagés, de la cartographie à l’énergie renouvelable, en passant par les connaissances et les sciences ouvertes ou les AMAPs et les épiceries coopératives, les « Communs » sont partout !

>En d’autres termes on peut définir les communs comme une ressource (bien commun) plus plus les interactions sociales (économiques, culturelles et politiques) au sein de la communauté prenant soin de cette ressource.

>On peut aussi définir les biens communs comme la recherche par une communauté d’un moyen de résoudre un problème en agissant au bénéfice de l’ensemble de ses membres.

>Il est important de noter que la définition des communs est un chantier à part entière toujours en cours, à l’image de leur diversité.
>Extrait du site lescommuns.org sous licence cc-by-sa
>

> Ce n'est pas la nature d'un bien qui en fait un commun mais la façon dont une communauté se rassemble pour le prendre en charge.
> Extrait de [l'exposition des communs](http://wiki.remixthecommons.org/index.php?title=Exposition_Les_communs) cc-by-sa 3.0


## Cadres juridiques de chacun et modèle de management existant et imaginable

Nous allons maintenant nous arrêter sur les différents modèles possibles pour imaginer une maison de santé comme un espace de gestion de la santé comme un commun.
Il s'agirait d'une structure permettant donc à une communauté de pouvoir prendre des décisions sur la gestion de ce bien qui est la santé.
Au delà des personnalités et volontés présentes, quelles structures juridiques et administratives permettraient de poser les bases d'un tel travail?

Nous allons nous attarder premièrement sur la Société Interprofessionnelle de Soins Ambulatoires (SISA).

Le [Protail d'Accompagnement des Professionnels de Santé](http://paps.sante.fr/index.php?id=520) nous indique ceci:

> * la mise en commun de moyens, comme pour une société civile de moyens (SCM) ;
> * la perception par la structure de rémunérations de l'Assurance Maladie de certaines activités exercées en équipe (comme la coordination autour de la prise en charge du patient) ;
> * le maintien de l'exercice, par les professionnels de santé, de leur activité en leur nom propre ;
> * la garantie de la transparence fiscale : les rémunérations perçues peuvent être réparties entre des associés qui les intègrent à leurs revenus et les déclarent comme tels.
> 
 Cette forme d'entreprise présente de nombreux avantages, et notamment le fait de mieux prendre en compte les (ex-)nouveaux modes de rémunération qui sont des modes de financements sur objectifs de santé publique. Ces objectifs sont définis avec l'Antenne Régionale de Santé et la maison de santé lors de l'établissement de son projet de santé. Vous pourrez trouver un peu plus d'informations sur le sujet [ici](https://www.santelog.com/actualites/msp-2016-annee-charniere-pour-les-nouveaux-modes-de-remuneration).
 La structure de type SISA, permet d'éviter l'assujettissement à l'impôt sur les sociétés pour les sommes perçues sur les NMR. Pour de plus amples informations, vous pouvez visiter [ce lien de l'ars sur le sujet](http://www.dpi.ars.sante.fr/fileadmin/PORTAIL/PDF/Questions_reponses_SISA_07_04_12.pdf).
 
Il est possible de créer une association avec les mêmes associés que la SISA, et d'y incorporer des usagers/bénévoles pour promouvoir les actions de santé et réfléchir ensemble à notre santé. Mais l'association doit être différente de la SISA (juridiquement parlant) si la structure souhaite toucher les NMR.

De ce que j'ai pu lire on peut donc imaginer:

 * une [SCIC](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_coop%C3%A9rative_d%27int%C3%A9r%C3%AAt_collectif) regroupant sous son toît:
     * une SISA pour la maison de santé
     * et différentes associations gravitant autour d'un projet commun d'intérêt collectif:
         * association d'usagers de la maison de santé
         * associaiton de promotion et d'expérimentation de techonologies ouvertes/libres en santé
         * association de promotion de nutrition saine avec formations au potager
         * etc.

Il ne restera plus qu'à inscrire dans les status des modes de gouvernance permettant de travailler avec la santé comme un commun. Et ces formes administratives favorisent ce type de gouvernance et de management.

On peut très bien imaginer du coup:

 *  des équipes de soignants rémunérés pour prévenir plus que pour guérir
 *  mieux intégrer les pouvoirs publics et la population dans les choix de santé publique locale
 *  des soignants et ingénieurs créer des outils de santé libre de demain et participer à ceux existants ([GliaX](https://github.com/GliaX/Stethoscope), [echopen](http://echopen.org/), etc.)
 *  des outils informatiques libres et respectueux de la vie privée pour gérer cette structure

Voir même une évolution vers des partenariats de type [public-commun](https://wiki.p2pfoundation.net/Vers_des_partenariats_Public-Communs) au niveau local (comme ce qui peut se faire au [port de Capri](http://www.labgov.it/2017/08/10/the-port-of-capri-public-private-commons-partnership/) et ailleurs).


## Conclusion

Cet article a été écrit à partir de quelques discussions avec des professionnels de santé, un directeur de clinique et beaucoup de recherche en ligne. Il s'inscrit donc plutôt comme un départ vers plus de recherche et de discussions jusqu'à aboutri à la création d'une maison de santé qui participerait à la gestion de la santé comme d'un commun. 

Les [déterminantes de santé](http://inpes.santepubliquefrance.fr/10000/themes/ISS/determinants-sante.asp) sont en effet nombreux et ce n'est que par une gestion transversale et commune que nous pourrons inspirer à un meilleur [état de santé](https://fr.wikipedia.org/wiki/Sant%C3%A9), voir même une nouvelle définition de la santé.
