+++
date = "2017-05-24T22:48:00+02:00"
draft = false
description = "Stéthoscope Open Source"
meta_img = "/images/image.jpg"
tags = ["stéthoscope","open hardware","open source"]
title = "Gliax: un stéthoscope open-source"

+++


Et si je vous disais que pour un coût de production de **2,5 à 5 dollars** vous pouvez monter votre stéthoscope à la maison ou avec votre hackerspace favoris?
Et si en plus de ça je vous disais que ce stéthoscope 'maison' est aussi performant qu'un stéthoscope utilisé par un cardiologue à l'hôpital?

>\o/ Ouah, mais c'est fou! J'en veux un!

>\o/ Pour ma part je suis hyper sceptique, ça peut pas être vrais.

**Calmez vous!**

Laissez moi vous présenter ce projet du [Dr Tarek Loubani](https://github.com/tareko).

Il s'agit d'un stéthoscope open-source à imprimer et à monter. La liste des matériaux et les pièces à imprimer sont [disponibles ici](https://github.com/GliaX/Stethoscope).

Vous avez ici un bref reportage d'euronews sur le sujet:


{{< youtube PzxdKxZY8uE >}}

## Philosophie du projet

> We make high-quality low-cost medical devices that use a Free/Libre Open Source (FLOS) philosophy to create hardware that is easily accessible and can be manufactured in low-resource settings.
>[GliaX](https://glia.org/#philosophy)

C'est un des point majeurs de l'open-source et des communs en santé: l'accessibilité!

Même si certains matériaux de santé et médicaments peuvent être accessibles en France, ils peuvent ne pas l'être simplement pour une question de coût ailleurs.
Evidemment ce coût final ne prend pas en compte que le coût matériel, mais aussi la R&D, le marketing, les brevets, etc.

Je ne connais pas le modèle économique de ces projets, ni même s'il y en a un, mais plusieurs sont imaginables. Ce n'est peut-être pas aussi lucratif que vendre des molécules à plusieurs dizaines de milliers d'euros, mais ça a le mérite d'être accessible (il faut tout de même avoir accès à de l'impression 3D) et d'avoir un impact cliniquement significatif dans la prise en charge et le soin de tous.

On pourrait, par exemple, faire monter et imprimer son stéthoscope 3D par un [hackerspace du coin](https://wiki.hackerspaces.org/List_of_Hacker_Spaces) contre rétribution, et une partie de largent irait au hackerspace et une partie au projet (à une personne ou un groupe de personnes).


## Technique

Le projet publie [les tests acoustiques](https://github.com/GliaX/Stethoscope/blob/master/Testing/Stethoscope_Validation.md) de ce stéthoscope ainsi que leur méthodologie.

![](https://github.com/GliaX/Stethoscope/blob/master/Testing/ModelSetup.PNG?raw=true)

Et la différence semble ténue voir nulle entre le GliaX et le Littmann cardiology III.

![](https://github.com/GliaX/Stethoscope/blob/master/Testing/Litt3vsGlia2.png?raw=true =400x400)


## Où j'en suis

Impressioné par ces résultats et trouvant ça trop classe de pouvoir réaliser un stéthoscope soi-même j'ai commencé à imprimer les pièces.


![](https://kapu.fr/jirafeau/f.php?h=2oKI_aRG&p=1)

J'ai pour cela été aidé du [nicelab](http://nicelab.eu/) et du hackerspace d'[evaleco](http://www.evaleco.org/).

Petit détail sur l'impression: je n'ai pour le moment pas réussi à écouter un coeur via le Gliax. Et j'ai mis du temps à en comprendre la cause pourtant évidente. Les paramètres d'impression ont fait que des espaces libres étaient laissés entre certaines travées, ainsi tout le son passe par là et non pas au travers des tuyaux.

Pour vérifier si les pièces sont relativements imperméables, il suffit d'en boucher une extrémité et de souffler dans l'autre. Si on trouve une résistance infranchissable, c'est gagné! La pièce est suffisamment imperméable et s'il y a un problème il vient d'ailleurs.

Pour le moment le stéthoscope est en pause jusqu'au [21 juin](http://cng.sante.fr/Epreuves-classantes-nationales-393). Je reposterai quelque chose ici si j'arrive à le faire fonctionner voir même à proposer des améliorations!

En attendant, je vous souhaite une bonne journée et j'espère que cet article vous aura plu.
