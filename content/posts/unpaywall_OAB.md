+++
date = 2017-10-11T18:06:52+02:00
draft = false
meta_img = "/images/image.jpg"
tags = ["open-science","open-access"]
description = "Desc"
title = "Unpaywall et Open Access Button"
+++

> "Comme le savoir c'est le pouvoir, et je vais vous donner tout le pouvoir"

>Un gentil monsieur à l'accueil du CROUS de Nantes.

Je vais vous présenter aujourd'hui 2 outils en faveur de l'open-data et open-source:

 * [Unpaywall](http://unpaywall.org/welcome)
 * [Open Access Button](https://openaccessbutton.org/)

## Unpaywall


![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/unpaywall.png)


Unpaywall est une extension pour Firefox et Chromium (Chrome) gratuite et open-source qui permet d’accéder gratuitement et légalement à de nombreux articles scientifiques.

Elle est développée et maintenue par une organisation à but non lucratif: Impactstory.
Ce projet a démarré lors d'un hackaton pendant le Beyond Impact en 2011 et a reçu de nombreux prix depuis.

Il ne vous demande aucune donnée personnelle et vous n'avez rien à payer. Il va chercher les documents dans les revues proposant déjà les articles en open-access comme [PlosOne](http://journals.plos.org/plosone/), mais aussi PubMed Central, Google Scholar et les articles directement publiés par leurs auteurs.

Il est en effet possible que même si article soit d'accès payant dans une revue, l'auteur ait le droit de le diffuser via un blog personnel par exemple (vous pouvez voir ça [ici](https://journals.aps.org/copyrightFAQ.html#post)).

**Je vais maintenant vous montrer comment s’en servir !**

Pour l'installer il vous suffit d’aller sur le site d’[unpaywall](http://unpaywall.org/) et de clicker sur: _ajouter à firefox (ou chrome)_.

Vous pouvez aller sur la page d'une revue scientifique, par exemple celui-ci: [Seven temperate terrestrial planets around the nearby ultracool dwarf star TRAPPIST-1](https://www.nature.com/nature/journal/v542/n7642/full/nature21360.html?foxtrotcallback=true#access) publié sur Nature.

Vous pouvez voir sur la droite de votre écran un petit verrou blanc sur fond vert.

![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/article_science.png)

Si le verrou est ouvert (comme ici) c’est que vous pouvez passer outre le 'paywall'. C’est-à-dire que vous pouvez accéder à l’article légalement sans avoir à payer.
Vous n’avez plus qu’à cliquer sur ce petit verrou, et vous avez accès à votre article!

![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/seven_trappist.png)

*C’est simple comme bonjour !*

## Open Access Button

![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/OAB%20search.png)

Comme vous pouvez le voir, **Open Access Button (OAB)** se présente sous la forme d'une barre de recherche où vous pouvez copier/coller les références d'un article (URL, DOI, PMID, etc.).

OAB va ensuite chercher si l’article n’est pas déjà présent gratuitement et légalement sur plusieurs bases de données. Et si ce n'est pas le cas, une requête peut être effectuée envers l'auteur principal. On peut ainsi demander l’accès à l'article, mais aussi aux données de l'étude.

L'équipe d'OAB ajoute aussi des articles dans [Zenodo](https://zenodo.org/) et les données dans l'[open science framework](https://osf.io/).

Pour vous en servir vous avez deux possibilités, soit copier/coller l'identifiant d'un article ou son URL dans la barre de recherche sur *openaccessbutton.org*, puis lancer la recherche, vous avez ainsi ce résultat:

![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/OAB_success_search.png)

Ou alors, vous pouvez avoir une 'extension' au sein de votre navigateur.Il faut pour cela aller sur *get the extension* puis se créer un compte (une adresse mail suffit), vous arrivez alors sur cette page:

![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/OAB%20drag%20n%20drop.png)

Vous devez ensuite faire un glisser/déposer depuis le bouton 'Open access button' jusqu’à votre barre de favoris.
Et **paf!** Vous avez l'extension, vous n'aurez plus qu'à cliquer dessus lorsque vous serez sur la page d'un article.

![](https://framagit.org/kapu/kapu.frama.io/raw/master/pictures/article%20unpaywall%20et%20OAB/OAB%20success.png)
Vous voyez qu'ici vous pouvez accéder à l'article. Si vous souhaitez accéder aux données vous pouvez lancer une requête.




Voilà une petite présentation de deux outils open-source que j'ai découvert récemment en faveur de l'open-access.

Tout citoyen - une fois connectés à internet - peut ainsi avoir un égal accès aux études scientifiques, ce qui est une des briques vers une prise en compte de la santé comme un commun.

Je ne vous en ait pas parlé ici, mais il existe bien sûr [Sci-Hub](https://sci-hub.ac/) qui n'est pas légal et dont l'équipe d'impactstory [parle sur son blog](http://blog.impactstory.org/comparing-sci-hub-oadoi/).

Il y a aussi [kopernio](https://kopernio.com/) mais dont le code n'est pas open-source, alors je ne me suis pas autant penché sur le sujet. Je trouve ça plus logique d'utiliser des outils open-source pour favoriser l'open-science.

Il existe aussi bien sûr la revue [Plos One](http://journals.plos.org/plosone/) qui publie ses articles en open access.

Pour conclure, il est important de rester alerte sur ces thématiques, l'open-access est une nécessité, et l'[europe](https://ec.europa.eu/programmes/horizon2020/en/h2020-section/open-science-open-access) semble s'en saisir, mais tout n'est pas gagné avec les traités de libre-échanges [1].
Et en guise d'ouverture je vous invite à parcourir l'excellent blog de Scinfolex, notamment sur le [thème de la recherche](https://scinfolex.com/tag/recherche/).

[1] Traité commercial États-Unis d'Amérique-Union européenne : menaces sur la santé Rev Prescrire 2015 ; 35 (377) : 218-219    
