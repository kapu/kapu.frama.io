+++
date = "2017-06-01T09:53:32+02:00"
description = "Desc"
draft = false
meta_img = "/images/image.jpg"
tags = ["science","open-source","LCA","ECN"]
title = "Controverse Sciences: de la science participative"

+++

{{< figure src="https://pbs.twimg.com/profile_images/776053108166238208/w-w9JeE__400x400.jpg" title="La grenouille de controverse sciences" >}}



[Controverse Sciences](https://www.controversciences.org/) est une plateforme open-source dont l'objectif est de répondre aux questions controversées du quotidien avec une méthode scientifique.

{{< youtube vqcBaFeAE64 >}}
A partir de 23 minutes 46 secondes.


> Les OGM sont-ils nocifs pour la santé?
> La lutte biologique peut-elle se substituer à l'utilisation des pesticides?

Le fonctionnement de la plateforme est très bien expliquée sur la vidéo.

Concrètement, une fois inscrit, les différents rôles que vous pouvez jouer au sein des controverses vous est expliqué au travers d'une bande dessinée!

Puis si vous souhaitez participer à un débat vous pouvez:

 * proposer des articles
 * synthétiser des articles
 * synthétiser ou reformuler la question du débat
 * synthétiser les réponses à la question posée
 * émettre votre avis
 * etc.

Une démarche scientifique et collaborative pour répondre aux controverses, le tout sur une plateforme open-source. Le top!


Et là où en tant qu'étudiant en médecine ça me parle, c'est que cette démarche se rapproche d'une de nos matières: la lecture critique d'articles (la LCA)!
Il s'agit d'une des épreuves à maîtriser pour les [épreuves classantes nationales](https://fr.wikipedia.org/wiki/%C3%89preuves_classantes_nationales) (alias les ECN). Il s'agit d'une matière ou nous sommes amenés à critiquer un article scientifique médicale (étude diagnostique, thérapeutique, etc.) le tout sous forme de QCM.

Bien que cette matière soit hyper intéressante elle présente quelques limites au niveau des ECN à mon avis. Et notamment par le format des qcms qui nous pousse assez peu à porter notre réflexion sur le texte mais plutôt à chercher à trouver la bonne réponse à une question posée. La démarche n'est pas la même et on n'en retient pas la même chose je trouve.

Et c'est là que Controverse Sciences débarque! Par la recherche, la synthèse et la critique des articles cette plate-forme serait un atout essentiel dans la pédagogie appliquée à la LCA: on travail sur des sujets d'actualité, on est amené à réfléchir et à aiguiser notre esprit critique puis  émettre un opinion (vais-je ou non prescrire ce nouveau traitement en connaissant les forces et faiblesses de cette étude?). Et en plus en plus cette démarche nous sensibilise aux forces et faiblesses des [méta-analyses](https://fr.wikipedia.org/wiki/M%C3%A9ta-analyse), choses auxquels nous sommes peut sensibilisé pendant l'externat.

Bien sûr ce n'est pas la seule forme imaginable, nous avons expérimentés au sein de l'[Esprit Critique Niçois](http://espritcritiquenicois.org/) des formes participatives de la LCA: débats autour du film du Dallas Buyer Club avec des études de l'époque, des petits groupes pour étudier des textes un peu sous la forme de tutorat.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Dallas_Buyers_Club.png/640px-Dallas_Buyers_Club.png "logo du film le dallas buyer club")


Voilà, il ne vous reste plus qu'à aller débattre sur [controverse sciences](https://www.controversciences.org/), et pourquoi pas sur: l'utilisation de logiciels libre en santé améliore la prise en charge des patients? Wikipedia est un atout à la pédagogie médicale? Un système de santé en commun améliorera notre espérance de vie?
