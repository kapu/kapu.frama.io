+++
date = "2017-05-13T22:55:29+02:00"
description = "Desc"
meta_img = "/images/image.jpg"
tags = ["Projets"]
title = "Projets"

+++

Liste des projets auxquels je participe ou j'ai participé.

## LibreHealthCare

LibreHealthCare est un collectif dont l'objectif est de promouvoir les standards ouverts, le matériel et le logiciel libre et open source dans le domaine de la santé.


[![](https://librehealthcare.org/img/dessin-1.png)](https://librehealthcare.org/)


## Esprit Critique Niçois

Brièvement, c'est un collectif d'étudiants en médecine à Nice qui cherche à stimuler l'esprit critique dans le domaine de la santé.

Nous avons organisés ça lors de deux journées à la faculté de médecine de Nice autour d'ateliers participatifs.

Si ça vous intéresse, une synthèse de ces journées a été publiée dans le journal de l'espace éthique azuréen [ici](https://www.espace-ethique-azureen.fr/wp-content/uploads/2017/09/EEA-Lettre-Hors-serie-No9-V05.pdf).

[![](https://espritcritiquenicois.files.wordpress.com/2016/10/banniere3.png)](https://espritcritiquenicois.org/)

## Incroyable Campus Valrose

Un jardin comestible ouvert au sein de la faculté des sciences à Nice. Le fonctionnement est calqué sur celui des incroyables comestibles.

[![](https://incroyablecampusvalrose.files.wordpress.com/2016/06/icv_test_petite1.png?w=600&h=600&crop=1)](https://incroyablecampusvalrose.org/)

## Nicelab

Hackerspace niçois où j'ai aidé à organiser des événements: contribunuits, soirée de discussion sur les notions d'internet et vie privée, etc.

[![](https://blog.nicelab.eu/wp-content/uploads/2014/01/cropped-banniere_nicelag_500x100.png)](https://blog.nicelab.eu/)

## Autres

 * Wikipedia bien sûr, surtout sur des articles en lien avec la médecine.
 * OpenStreetMap, via [StreetComplet](https://github.com/westnordost/StreetComplete) et [HOT OSM](https://www.hotosm.org/).

## En cours

 * Petit module pour simuler l'auscultation cardiaque afin de l'inclure dans les examens facultaires de médecine. Puis réaliser un serious-game sur ce thème.
 * Une carte interactive pour mieux mettre en valeur le [classement des facultés de médecine](http://facs.formindep.org/) réalisées par le formindep.

## Dons
Je me donne un budget mensuel pour aider à financer différents projets en faveur du libre comme [Framasoft](https://framasoft.org/), Wikipedia,[LQDN](https://www.laquadrature.net/) etc.

Mais aussi directement des développeurs via la plateforme de [liberapay](https://fr.liberapay.com).

J'en parle ici parce c'est en tombant sur un article de [korben](https://korben.info/) sur le financement de projets libres via un budget mensuel que je me suis mis à le faire. Je donnais déjà lors de campagne de dons, mais donner plus régulièrement en 'budgétisant' ça est plus intéressant pour les personnes qui maintiennent des projets ouverts sur long terme.
