+++
date = "2017-05-13T22:53:34+02:00"
description = "Desc"
meta_img = "/images/image.jpg"
tags = ["about"]
title = "A propos"

+++

Débuts de mon blog sur....la santé et les communs.

Actuellement étudiant en santé, les logiciels libres m'intéressent depuis plusieurs années. Je ne suis pas développeur, je sais me débrouiller avec un ordinateur et linux...je sais surtout poser des questions sur les forums!

Cependant, la notion de logiciel libre et son intérêt dans le milieu de la santé m'a très rapidement parlé, j'en ai discuté et même fais des [présentations en public](https://yolt06.github.io/#/step-1).

Et c'est maintenant une question un peu plus large qui me taraude: la notion de bien commun et de santé.
La santé est-elle un bien commun en France? Qu'est-ce qu'un bien commun?

J'ai pas mal remis en question mes études et ma volonté de faire médecine. J'ai continué tant bien que mal, et ce sont des exemples d'étudiants, de médecins, de projets en santé qui m'aident à me projeter vers une médecine que j'aimerai pratiquer.

Ce blog sera essentiellement là pour partager mes réflexions sur ce sujet, ainsi que les projets liés aux communs (logiciels libres, wikipedia, [HOT OSM](https://www.hotosm.org/), etc.) auxquels je participe ou qui me touchent.

{{< figure src="https://framapic.org/HjO4I9m4mIP7/5OKmcexr7Fza.jpg" title="La route est longue mais la voie est libre" class="img-responsive center-block" >}}


