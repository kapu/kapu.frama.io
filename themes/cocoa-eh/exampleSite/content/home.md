+++
title = "Home"
+++

Bienvenue sur ce blog d'un étudiant en santé passioné des logiciels libres et avide d'explorer le monde des communs en général.
